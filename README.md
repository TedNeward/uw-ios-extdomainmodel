# Greetings!

This exercise is designed to test your ability to work with some of the more object-extension
facilities of the Swift programming language.

Your task here is a little more complicated than before, but still conceptually simple: you will
branch your "simpledomainmodel" project, add some tests from this project to it, and then make
the tests pass. (This is partly to help you spend some time with Git branching.)

## Branch
TODO

## Tasks
You will do three things as part of this homework: implement a protocol on all your types;
define a new protocol for Money; create an extension for Double to make it easy to convert
a Double into an appropriate Money type.

### Implementing a protocol
For each of the classes and structs you worked on (Money, Job, Person, Family), implement
the CustomStringConvertible protocol. This means adding a `description` property of type
String that returns a human-readable version of the contents of the object. Make sure the
contents of each object are displayed.

### Define a protocol
Define a new protocol called `Mathematics`. It should contain two math methods (`add` and
`subtract`) ... TODO

### Define an extension
Write an extension to extend the Int type in Swift. Add four new extension properties,
called `USD`, `EUR`, `GBP` and `CAN`, and have each return a new Money instance containing
the Int's value as the `amount` in the Money.




